package multiplayer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import GameOnline.CannonBallOnline;
import GameOnline.CannonOnline;
import GameOnline.PairOnline;
import GameOnline.PlayerOnline;
import GameOnline.StatsOnline;

public class PiratePrimaryServer implements Runnable {
    
    private final int PORT = 5888;
    private Thread t;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private BufferedReader in;
    private boolean logicSocketCreated;
    private LogicContainer logicContainer;
    int y = 150;
    private static int id = 0;
    
    public LogicContainer getLogicContainer (){
    	if (logicContainer == null)
    		logicContainer = new LogicContainer();
    	return logicContainer;
    }
    
    public PiratePrimaryServer() 
    {
        try 
        {
            t = new Thread(this);
            serverSocket = new ServerSocket(PORT);
        } catch (Exception e) {
            
        }
    }
    
    public void start() {
        t.start();
    }
    
    @Override
    public void run() {
        try {
            System.out.println("Server primario partito");
            while(true)             
            {                                
                clientSocket = serverSocket.accept();                
                if (!this.logicSocketCreated)
                {
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    PirateServerTCP subServer = new PirateServerTCP(clientSocket);
                   
                    PlayerOnline player = new PlayerOnline(new PairOnline (150, y), new PairOnline(60, 100), new StatsOnline (new CannonOnline (0, 1000, null), 150, 100, 70, 10, 8, 3));
        			player.getStats().getCannon().setCannonBall(new CannonBallOnline(20, 500, player));
        			y+=150;
                    player.setID(id++);
                    subServer.player = player;
                    
        			this.getLogicContainer().addClientInMap(clientSocket.getInetAddress().toString());
                    this.getLogicContainer().getGame().addPlayer (player);
                    this.getLogicContainer().addPirateServer(subServer);
                    
                    subServer.setLogicContainer (this.getLogicContainer());
                    subServer.start();
                    //this.logicSocketCreated = true;
                }
                else
                {                      
                    System.out.println("Qui ci sono arrivato");
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    PirateServerTCP subServer = new PirateServerTCP(clientSocket, true);
                    subServer.setLogicContainer(this.getLogicContainer());
                    subServer.start();
                    //this.logicSocketCreated = false;
                }    
                
                this.logicSocketCreated = !this.logicSocketCreated;
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
        try {
            serverSocket.close();
        } catch (Exception e) {
            
        }
    }
}
