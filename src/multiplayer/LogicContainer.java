package multiplayer;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

import GameOnline.ExplosionOnline;
import GameOnline.GameOnline;
import GameOnline.PirateTimerOnline;
import GameOnline.PlayerOnline;
import GameOnline.InfoOnline.Information;

public class LogicContainer {
	
	HashMap<String, Integer> findPlayer;
	GameOnline game;
    private Information info;
    CopyOnWriteArrayList <PirateServerTCP> pirateserver = new CopyOnWriteArrayList<PirateServerTCP>();
    private static int id = 0;
	
    public void addPirateServer (PirateServerTCP pirateServerTCP){
    	pirateserver.add(pirateServerTCP);
    }
    
    public synchronized void receiveAndSendEvents (String event, int _id){
    	this.eventsAndIdReceived(event, _id);
    	this.notifyAllClients();
    	System.out.println("EVENTO: " + event + " ID: " + _id);
    }
    
    public void eventsAndIdReceived (String event, int _id){
    	if (game != null) {
    		/*if (event.equals("Click"))
	    		for (PlayerOnline player : game.players){
	    			if (player.getID() == _id){
	    				player.getStats().setHealth_point(player.getStats().getHealth_point() - player.getStats().getCannon_attack() / player.getStats().getCannon_defense() + 10);
	    				game.explosions.add(new ExplosionOnline(player.getPosX() + player.getData().getX()/2, player.getPosY() + player.getData().getY() / 2));
	    			}
    		}
		    else{*/
    		if (event.equals("Space")){
    			Random rndm = new Random ();
    			int i = rndm.nextInt(game.players.size());
    			game.players.get (i).getStats().setHealth_point(game.players.get (i).getStats().getHealth_point() - game.players.get (i).getStats().getCannon_attack() / game.players.get (i).getStats().getCannon_defense() + 10);
				game.explosions.add(new ExplosionOnline(game.players.get (i).getPosX() + game.players.get (i).getData().getX()/2, game.players.get (i).getPosY() + game.players.get (i).getData().getY() / 2));
				return;
    		}
			  	for (PlayerOnline player : game.players) {
				   	if (player.getID() == _id){
				   		int i = PirateServerTCP.eventsHandler(event);
				    		if (i != -1)
				    			player.keyPressed(i);
				    			}
			    		}
		    	//}
    	}
    }
    
    public void notifyAllClients (){
    	for (PirateServerTCP pirateServerTCP : pirateserver) {
			pirateServerTCP.sendGameLogic();
		}
    }
    
	public LogicContainer() {
		findPlayer = new HashMap<>();
		//game = new GameOnline(0);
	}
	
	public void addClientInMap (String address){
		findPlayer.put(address + id, id);
		id++;
		System.out.println("Aggiunto all'hash: " + id);
	}
	
	public void getID (String address){
		for (int i = 0; i < id; i++) {
			String s = address + i;
			int j = this.findPlayer.get(s);
			System.out.println("id: " + j);
			System.out.println("address: " + address);
		}
	}
	
	public GameOnline getGame (){
    	if (game == null){
    		info = new Information ();
    		game = new GameOnline (0);
    		//Information.player.setScreen(game.screen);
    		startGame ();
    	}
    	return game;
    }
	
	 public void startGame (){
	    	PirateTimerOnline pt = new PirateTimerOnline();
	        TimerTask tt = new TimerTask() {
				
				@Override
				public void run() {//System.out.println("REFRESH");
					game.check();
					game.refresh();
				}
			};
			pt.schedule(tt, 90, 90);
	    }
	 
	 
}
