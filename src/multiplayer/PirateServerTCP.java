package multiplayer;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.TimerTask;

import GameOnline.GameOnline;
import GameOnline.PirateTimerOnline;
import GameOnline.PlayerOnline;
import GameOnline.InfoOnline.Information;


public class PirateServerTCP implements Runnable
{   
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private PiratePrimaryServer primaryServer;
    private Thread t;
    private OutputStreamWriter osw;       
    private boolean isEventReceiver; 
    private ObjectInputStream objectPrinter;
    private ObjectOutputStream objectWriter;
    private static int ipPlus;
    //private example ex = new example ();
    private LogicContainer logicContainer;
    PlayerOnline player;
    
    public void setLogicContainer (LogicContainer _logic){
    	logicContainer = _logic;
    }
    
    public ObjectOutputStream getObjectWriter() {
    	if (objectWriter == null){
			try {
				objectWriter = new ObjectOutputStream(this.socket.getOutputStream());
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
		return objectWriter;
	}

	public ObjectInputStream getObjectPrinter (){
    	if (objectPrinter == null)
			try {
				objectPrinter = new ObjectInputStream(this.socket.getInputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	return objectPrinter;
    }
    
    public PirateServerTCP(Socket s) {
        socket = s;
        t = new Thread(this);
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            osw = new OutputStreamWriter(socket.getOutputStream());
            out = new PrintWriter(new BufferedWriter(osw), true);
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
    public PirateServerTCP(Socket s, boolean isEventReceiver) 
    {
        this.isEventReceiver = isEventReceiver;
        socket = s;
        t = new Thread(this);
        try 
        {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            osw = new OutputStreamWriter(socket.getOutputStream());
            out = new PrintWriter(new BufferedWriter(osw), true);
        }
        catch (Exception e) 
        {
            System.err.println(e);
        }
    }
    
    public void start() 
    {
        t.start();
    }
    
    @Override
    public void run() 
    {
        String s = this.isEventReceiver ? "game events receiver" : "game logic sender";
        System.out.println("Server secondario partito, sono un: " + s);
        
        if (this.isEventReceiver)
        {
            this.gameEventsReceiver();
        }
        else
        {
            this.gameLogicSender();
        }
    }
    
    public void sendGameLogic (){
	   	 try {
			getObjectWriter().writeObject(this.logicContainer.getGame());
		   	 this.getObjectWriter().reset();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void gameLogicSender()
    {
        try 
         {
             String line = in.readLine();        
             if (line.equals("SS"))
             {
                 out.println(player.getID());
                 System.out.println("Serve invia ID: " + player.getID());
                 
                 line = in.readLine();
                 if (line.equals("SSSS"))
                 { 
                     /*while(true)
                     {
                    	 Pirate
                    	 this.sendGameLogic();
                     }                    
                    */ //TODO: da qui deve partire un altro server con un altro client
                 }
             }
         } catch (Exception e) {
 			System.out.println("NEL SERVER 3:");}
    }
    

	private void gameEventsReceiver()
    {
        try 
         {
            /* String line = in.readLine();        
             if (line.equals("SS"))
             {
                 out.println("SSS");
                 */
                 while(true)
                 {
                    this.eventReceiver();
                 }
            // }
         } catch (Exception e) {}
    }
    
    public void eventReceiver (){
    	try {
			String s = (String) this.getObjectPrinter().readObject();
	    	System.out.println("Server Riceve: " + s);
	    	String[] events = this.getEventAndID(s);
	    	for (int i = 0; i < events.length; i++) {
				System.out.println(events [i]);
			}
	    	this.logicContainer.receiveAndSendEvents(events[0], Integer.parseInt(events[1]));
	    	//eventsHandler(s);
	    	
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public String[] getEventAndID (String s){
    	return s.split("£");
    }
    
    public static int eventsHandler (String event){
    	System.out.println("EVENTS HANDLER");
    	int key = -1;
    	if (event.equals("Up")){
    		key = KeyEvent.VK_UP;
        	System.out.println("UP");
    	}
    	else if (event.equals("Right")){
    		key = KeyEvent.VK_RIGHT;
    		//game.getPlayer().keyPressed(key);
        	System.out.println("Right");
    	}
    	else if (event.equals("Left")){
    		key = KeyEvent.VK_LEFT;
    		//game.getPlayer().keyPressed(key);
        	System.out.println("Left");
    	}
    	else if (event.equals("Down")){
    		key = KeyEvent.VK_DOWN;
    		//game.getPlayer().keyPressed(key);
        	System.out.println("Down");
    	}
    	else if (event.equals("Esc")){
    		key = KeyEvent.VK_DOWN;
    		//game.getPlayer().keyPressed(key);
        	System.out.println("ESC");
    	}
    	else if (event.equals("Click"))
        	System.out.println("Click");
    	
    	return key;
		//System.out.println("POS: " + Information.player.getPosX());
    }
    
}
